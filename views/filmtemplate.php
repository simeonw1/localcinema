

  <dd class="accordion-navigation">
    <a href="#<?=@$this->panelName?>"><?=@$this->accordionName?></a>
    <div id="<?=@$this->panelName?>" class="content <?=@$this->accordionClass?>">
	    
	<div class="row">

	  <div class="small-12 medium-12 large-12 columns">
		  
      <?=@$this->showings?>
	  
	  </div>

<!-- 		  Show for small -->
	  <div class="small-6 columns">
		  <span class="show-for-small-only">
	      
			  <div style="background-color: rgb(<? print($this->rgb)?>); width: auto;">
			  <strong><?=@$this->rating?></strong> from<?=@$this->votes?><br/>
			  <?=@$this->urlIMDB?>
			  </div>
		  </span>	
	  </div>
	  <div class="small-6 columns">
		  <span class="show-for-small-only">
	      <img src="<?=@$this->poster?>" alt="<?=@$this->title?>" title="<?=@$this->title?>"><br/>
		  </span>
	  </div>
<!-- 		  Show for small -->


	  <div class="medium-4 large-4 columns">

      	  <span class=" hide-for-small-only">
		    <div style="background-color: rgb(<? print($this->rgb)?>); width: auto;">
			  
		    <strong><?=@$this->rating?></strong> from<?=@$this->votes?><br/>
		    <?=@$this->urlIMDB?>
		    </div>
		    <img src="<?=@$this->poster?>" alt="<?=@$this->title?>" title="<?=@$this->title?>"><br/>
	        <?//=@$this->poster?>
		  </span>	
	  </div>


	  <div class="small-12 medium-8 large-8 columns">
	  
<!--       <blockquote> --><?=@$this->simplePlot?>
<!-- </blockquote> -->
 
 <!--|||||||||MODAL||||||||||||-->							
				<a href="#" data-reveal-id="videoModal<?=@$this->panelName?>" class="radius button">Watch the Trailer&hellip;</a>
				<div id="videoModal<?=@$this->panelName?>" class="reveal-modal large " data-reveal aria-labelledby="videoModal<?=@$this->panelName?>Title" aria-hidden="true" role="dialog">

  <div class="flex-video widescreen vimeo">

<!-- <iframe src="http://www.imdb.com/video/imdb/vi1462938649/imdb/embed?autoplay=false&width=480" width="480" height="270" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" frameborder="no" scrolling="no"></iframe>
 -->

<?
//https://www.videodetective.com/embed/video/?publishedid={INSERT PUBLISHEDID}&options=false&autostart=false&playlist=none
//http://v.traileraddict.com/<?=@$this->trailer['id']?>
	  <iframe width="640" height="400" src="<?=@$this->trailer['link']?>/imdb/embed?autoplay=false&width=640 allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" scrolling="no"></iframe>	<div><a href="<?=@$this->trailer['link']?>">Trailer</a>.</div>
	  
<!--  <iframe width='640' height='480' src='<?=@$this->trailer['link']?>' frameborder='0' scrolling='no'></iframe>  -->
  </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>

<!--||||||||||MODAL|||||||||||-->	
 
      
	  </div>

	</div>
<!--
	<div class="row">
	  <div class="large-6 columns">
			<?=@$this->mapTemplate?>
	  </div>
	</div>
-->

	<div class="row">
	  <div class="small-12 medium-12 large-12 columns">
			
	  </div>
    </div>
  </dd>

	