
<section id="mappage<?=@$this->panelName?>">

	
	<div class="row">
		<script>
L.mapbox.accessToken = '<?=MAPBOX_KEY?>';
var map = L.mapbox.map('mappage<?=@$this->panelName?>', 'examples.map-i86nkdio', {
})
		    .setView(<?=@$this->latlng?>/* [51.507, -0.116] */, 14);
		
		L.tileLayer('http://{s}.tiles.mapbox.com/v3/simeo.m07cg9aa/{z}/{x}/{y}.png', {
		    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
		    maxZoom: 16
		}).addTo(map);


// Disable drag and zoom handlers.
map.dragging.disable();
map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();

// Disable tap handler, if present.
if (map.tap) map.tap.disable();

		var circle = L.circle(<?=@$this->latlng?>,15, {
		//var circle = L.circle([51.508, -0.11], 50, {
		    color: 'red',
		    fillColor: '#f03',
		    fillOpacity: 0.5
		}).addTo(map);
		

		circle.bindPopup("<?=@$this->title?>");

	</script>

	</div>
</section>