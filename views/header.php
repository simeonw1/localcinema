<!doctype html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1,user-scalable=no" />
    <title><?=$this->siteTitle//SITETITLE?></title>

<?
	
$URL = URL;
$ROOT = ROOT;	
?>
	<link rel="stylesheet" href="<?php echo URL.APPS;?>localcinema/public/css/app.css" media="all">

<? //echo file_get_contents($URL.'public/css/app.php?url='.$URL.'&root='.$ROOT); 
		
	//	if ($jsonData === FALSE) {
	//	print "FAIL";
	//	} else {
	//		print 'succ';
	//	print $jsonData;	
	//	}
	?>
	
	<?/*
		<link rel="stylesheet" href="<?=URL;?>public/foundation/css/app.css//public/css/app.php?>" media="all">   
		*/?>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.6/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.6/mapbox.css' rel='stylesheet' />
<script src='https://maps.googleapis.com/maps/api/js?libraries=geometry'></script>

<?=@$this->extraCss?>
  </head>
  <body>
	  
	      
		<div class="fixed contain-to-grid" style="z-index: 10">		


		  
		  
		  		<!-- Positioning the Top Bar -->
		
		 <nav class="top-bar" data-topbar data-no-turbolink role="navigation">
		  <ul class="title-area">
		    <li class="name">
		      <h1><a href="<?=URL.APP_NAME?>"><?=$this->siteTitle//SITETITLE?></a></h1>
		    </li>
		     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
		    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		  </ul>



		
		<!-- <nav> goes here -->


		
		  <section class="top-bar-section">
		    <!-- Right Nav Section -->
		    <ul class="right">
			  <li><a href="<?=URL.APP_NAME?>about">About Us</a></li>
			  <li class="has-dropdown"><a href="#">Site</a>
			     <ul class="dropdown">
				   <li><a href="<?=URL.APP_NAME;?>index">Home</a></li>
				   <li><a href="<?= URL.APP_NAME;?>"><?=$this->siteTitle?></a></li>
				      <?php if (session::get('loggedin') !== null) {; ?>
					  	<li><a href="<?= URL;?>user/~<?=session::get('login')?>">User</a></li>
					  <? }?>
			     </ul>
			  </li>   

		    <?php if (session::get('loggedin') !== null) {; ?>
				<?//=@$this->navLinks;?>
				<?
		$this->l = new links();
		$this->l->setOffset(11,-4);
	
		$pages = $this->l->getLinks('controllers');
?>
		      <li class="active has-dropdown">
		        <a href="#">Installed Pages</a>
		        <ul class="dropdown">
<?
			foreach ($pages as $item){
				?>
				<li><a href="<?=URL.$item;?>"><?=$item?></a></li>
			<?
			}
					
			?>
			
				</ul>
		      </li>

		      <?php } ?>


		    </ul>
		
		    <!-- Left Nav Section -->
		    <ul class="left">
		    <?php if (session::get('loggedin') == null) {; ?>
			    <li class="has-form">
				  <div class="row collapse">

					<form action="<?=URL?>login/submit" method="post" name="loginform">
					    <div class="large-4 small-4 columns">
					      <input name="login" type="text" placeholder="Username" id="login" />
					    </div>
					    <div class="large-4 small-4 columns">
					      <input name="password" type="password" placeholder="Password" id="password" />
					    </div>
					    <div class="large-4 small-4 columns">
					      <input type="submit" value="Log In" class="alert button expand">
					    </div>
					</form>
				  </div>
				</li>


		      <?php } else { ?>
		      <li><a href="<?php echo URL;?>dashboard">Dashboard</a></li>		
			  <li><a href="<?php echo URL;?>logout">Logout</a></li>	
		      <?php } ?>
		    </ul>
		    
		    
		  </section>
		</nav>

		<!-- <nav> goes here -->
		</div>
<!--
	<section id="bodypanel">
	</section>
-->
