/****/
Each app slots into PHP-MVC when installed in to the apps directory

Features:
- MVC processing
- Local app libs and images
- Automated database creation adapted via the model		 

//////////////////////////
//// INSTALL TUTORIAL ////
//////////////////////////

cd into apps folder.

cd apps

git clone https://bitbucket.org/simeonw1/testapp.git appname

In the php-mvc libs directory, open 'bootstrap.php' and add the name of the app to the 'apps' array.

//////////////////////////
///Function Tutorial  ////
//////////////////////////

The bootstrap routes to apps as follows:

No params:
Loads the default controller e.g. index controller/index method

Default params:
/controller/method/param1/param2/param3/param4

App override:
/APPname[default-controller]/method/param1/param2/param3




//(C) Simeon Welby 2015