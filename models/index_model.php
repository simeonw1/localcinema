<?php

class index_model extends model {
	
public $status;
public $venues;
public $films;
public $timeFilter;
public $output;
public $areaCinemas;

  	public function __construct() {
		parent::__construct();
 			$this->userid = session::get('userid'); 
 			$this->timeFilter = date('H');  	
				
		if (ENVIRONMENT == 'installation'){
		$this->_install();	
		}
		
		//$this->resetVenueCount();
		$this->output = array();


	$this->imdbNo2 = new imdb(); 	
	}
	

		private function _install() {

		$this->dbtable = 'venue';

		$fields = "`venueid` int(11) NOT NULL AUTO_INCREMENT,
  `cinemaid` int(11) DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NULL,
  `phone_number` varchar(15) NULL,
  `link` varchar(255) NULL,
  `count` int(11) DEFAULT '1',
  PRIMARY KEY (`venueid`)";
  

			$this->install->buildTable($this->dbtable,$fields);

		$this->dbtable = 'film';

		$fields = "`filmid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `rated` varchar(15) NULL,
  `runtime` varchar(15) NULL,
  `rating` DECIMAL (2, 1),
  `votes` varchar(10) DEFAULT '1',
  `metascore` varchar(15) NULL,
  `releaseDate` varchar(15) NULL,
  `year` varchar(255) NOT NULL,
  `image_filename` varchar(100) NULL,
  `plot` TEXT NULL,
  `simple_plot` TEXT NULL,
  `id_imdb` varchar(30) NOT NULL,
  `url_imdb` varchar(255) NOT NULL,
  `retrieval_date` DATETIME DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`filmid`)";
   
			$this->install->buildTable($this->dbtable,$fields);

exit;
		}


	public function insertVenue($data) {	
		
		$this->db->insert('venue', array(
			'cinemaid' => $data['cinemaid'],
			'title' => $data['title'],
			'phone_number' => $data['phone_number'],
			'address' => $data['address'],
			'link' => $data['link'],
			'count' => 1
		));
				
	}

	public function getVenue($id) {	
		
		$r = $this->db->select("SELECT cinemaid,title,address,link,phone_number,count FROM venue where cinemaid = :cinemaid", array(':cinemaid' => $id));
		
		return $this->_returnData($r);
	}

	public function listTopVenues($limit) {	
		
		$r = $this->db->select("SELECT cinemaid,title,address,link,phone_number,count FROM venue ORDER by count desc LIMIT :limit", array(':limit' => $limit));
		
		$r = $this->_returnData($r);
		
		if($r == false) {
			return false;
			exit;
		}
		
				
		return $r;
		
	}
	public function resetVenueCount() {
		//print_r($data);
        $postData = array(	
			'count' => 1
        );
        
        $this->db->update('venue', $postData, "`count` > 1");
		
	}

	public function updateVenueCount($id) {	
		//print "Updating ".$id.'<Br/>';
		$count = $this->getVenue($id);
		if($count['0']) {
			$count = $count['0']['count']+1;
			} else {
				return false;
			}
			
			
		//print_r($data);
        $postData = array(	
			'count' => $count
        );
        
        $this->db->update('venue', $postData, "`cinemaid` = {$id}");		
		return true;
	}
	
/*
	public function getSingleVenue($this->var2) {
		
		
	}
*/

  
  	public function insertFilm($data) {	

	$postData = array();
	
	foreach($data as $key => $value)
  		$postData[$key] = $data[$key];
	
	$dt = date("Y-m-d H:i:s");
	$postData['retrieval_date'] = $dt;
						
		$this->db->insert('film', $postData);
				
	}

	public function updateFilmDate($id_imdb) {	


		//print "Updating ".$id.'<Br/>';

		if($id_imdb) {
			$count = $this->getFilm($id_imdb);	
			$cacheCount = $this->getFilm($id_imdb,date("Y-m-d",strtotime("-1 months")));
		} else {
			return false;
		}
//		Options = Yes within last month (leave). Yes older (update). No (false).

		//If there is a record, but there isn't one within the last month...
		if(isset($count['0']) && !isset($cacheCount['0'])) {
			//	$dt = date("Y-m-d H:i:s");
		//print_r($count);
		//exit;
		        $postData = array(	
					'retrieval_date' => date("Y-m-d H:i:s")
		        );

			//	print  print_r($postData, true).', `id_imdb` = {'.$id_imdb.'}';
				$this->db->update('film', $postData, "`id_imdb` = '$id_imdb'");		
			return true;
		        
			} else {
				return false;
			}

	}

	public function getFilm($id_imdb,$cacheDate = null) {			
		$cacheDate = isset($cacheDate) ? $cacheDate : date("Y-m-d",strtotime("-1 months"));
		
		$r = $this->db->select("SELECT title,rated,rating,runtime,votes,releaseDate,year,image_filename,plot,simple_plot,id_imdb,url_imdb,retrieval_date FROM film where id_imdb = :id_imdb AND retrieval_date >= :cacheDate  ORDER BY retrieval_date DESC LIMIT 1", array(':id_imdb' => $id_imdb,
	  ':cacheDate' => $cacheDate));
		
		return $this->_returnData($r);
	}

	public function listTopFilms($limit,$cacheDate = null) {	
		$cacheDate = isset($cacheDate) ? $cacheDate : date("Y-m-d",strtotime("-1 months"));
		
		$r = $this->db->select("SELECT title,rated,rating,runtime,votes,releaseDate,year,image_filename,plot,simple_plot,id_imdb,url_imdb,retrieval_date FROM film where retrieval_date >= :cacheDate ORDER BY rating DESC LIMIT :limit", array(':cacheDate' => $cacheDate,
	  ':limit' => $limit));
		
		
		$r = $this->_returnData($r);
		
		if($r == false) {
			return false;
			exit;
		}
		
				
		return $r;
		
	}


	public function updateFilmPoster($id_imdb,$filename) {	
		//print "Updating ".$id.'<Br/>';
			//$posterInfo['movie_results']['0']['poster_path'];
		//print_r($data);
        $postData = array(	
			'image_filename' => $filename
        );
        //print $id_imdb;
        
        
        $this->db->update('film', $postData, "id_imdb = '$id_imdb'");		
		return true;
	}

		
	private function _returnData($r) {
				
		if(isset($r[0])) {
			$this->r = $r;
		return $r;
		} else {
		return false;
		}
		
	}
	
	public function setTimeFilter($timeFilter) {
		$this->timeFilter = isset($timeFilter) ?  date('H')-$timeFilter : $this->timeFilter;
	}

	public function search($location = false) {
		
		$this->status = array();
			
			if(empty($location)) {
				$this->status[] = "No location selected.";
				return false;
				
			} else {
				$this->status[] = "Location selected:".$location;
				
				$string = "";
				$this->location = $location; //preg_replace('/\s+/', '', $location);
			}


			//Return postcode from non-postcode!
			$postcode_check = postcode::inspect($this->location);
			

			
			//LatLng from non postcode via google TODO: Caching.
			$latlng = postcode::getLatLng($this->location);
			$this->latitude = $latlng['latitude'];
			$this->longitude = $latlng['longitude'];
			$latlng = $this->latitude.','.$this->longitude;
			
/*
						//Retrieve lat / long for the postcode
						$postcode = new postcode();
						$postcode = $postcode->return_postcode($this->location);
						print helper::pr($postcode);
						$string .= "<h3>UK-Postcodes info: ".$postcode->data['postcode']."</h3>";	
						exit;	
*/		
			if($postcode_check['validate'] == true) { //location string = postcode format	
				return $postcode_check['prefix'].$postcode_check['suffix'];
				exit;
/*
			if (preg_match("(([A-Z]{1,2}[0-9]{1,2})($|[ 0-9]))", trim($this->location), $match)) {
				$this->partpostcode=$match[1];
				
				if(isset($this->partpostcode))
					return $this->partpostcode;
				exit;
			}
*/
			} else { //location = Not a postcode

/*TBC- return prefix only*/

				//print "not a postcode";
				//print $latlng;	
					if(isset($this->latitude) && isset($this->longitude)) {

					//Try get a postcode from lat lng.
					$postcode_object = new postcode(); //new obj
					$this->postcodeData = $postcode_object->return_postcode($this->location, $this->latitude,$this->longitude); 
					
					//print_r($this->postcodeData);
					//get postcode from non postcode
					if(isset($this->postcodeData['postcode']) && ($this->postcodeData['postcode'] !== false) && !@$this->postcodeData['error'])	$this->postcode = $this->postcodeData['postcode'];

					//print $this->postcode;
					if(isset($this->postcode)) {
						$postcode_check = postcode::inspect($this->postcode);
					if($postcode_check['validate'] == true) { //location string = postcode format	
						return $postcode_check['prefix'].' '.$postcode_check['suffix'];
						exit;
					}
//						return trim($this->postcode);
						} else {
							return false;
						}
					exit;
					}
		}
	return false;
	}

	


	public function getCinemas($postCode) {
		
	$options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');
	$cache = new cache($options);
	$cache->setLifeTime(8640000); //100 days
	
	
	
	
	//$qs = str_replace(' ', '+', $postcode);
		$url = 'http://moviesapi.herokuapp.com/cinemas/find/'.$postCode;
		$params = array();		
		$url = $cache->set_url($url,$params);
	$venueData = $cache->perform_cache($url,'cinema-'.$postCode); 


	if(isset($venueData['errmsg']) || isset($venueData['message'])) {
	//print_r($venueData);
	return false;
	exit;
	}

//print "Uncomment";
	//if(is_array($venueData))
	helper::array_sort_by_column($venueData, 'distance', SORT_NUMERIC);

	$string = "";
	$dbstring ="";
	$this->venues = array();
	

	// var_dump($venueData);
	// exit;

	foreach($venueData as $key => $value) {

		//Check output of the API in case named fields are different.
		$cinemaName = isset($venueData[$key]['title']) ? $venueData[$key]['title'] : "";
			if($cinemaName == "") {
				$cinemaName = isset($venueData[$key]['name']) ? $venueData[$key]['name'] : "";
			}

//		isset($venueData[$key]['name']) ? $venueData[$key]['name'] : "";
		$cinemaURL = isset($venueData[$key]['url']) ? $venueData[$key]['url'] : "";
			if($cinemaURL == "") {
				$cinemaURL = isset($venueData[$key]['link']) ? $venueData[$key]['link'] : "";
			}
		$cinemaPhone = isset($venueData[$key]['phone_number']) ? $venueData[$key]['phone_number'] : "";

			
			$this->venues[] = array('id' => $venueData[$key]['venue_id'], 'title' => $cinemaName);
					
	$check = $this->getVenue($venueData[$key]['venue_id']);

				//print	$venueData[$key]['venue_id'].print_r($check).'</br>';	

	//If there isn't one..
	if($check == false)	{
	$this->venues[$key]['0'] = array(
			'cinemaid' => $venueData[$key]['venue_id'],
			'title' => $cinemaName,
			'address' => $venueData[$key]['address'],
			'link' => urldecode($cinemaURL),
			'phone_number' => $cinemaPhone,
			'count' => 1
			);

		$this->db->insert('venue', $this->venues[$key]['0']);

		$string .= "<h4><a href=\"".URL."localcinema/cinema/".$venueData[$key]['venue_id']."\">".$cinemaName."</a></h4>";
		//$string .= "<a href=\"".URL."localcinema/cinema/".$venueData[$key]['venue_id']."\">What's on!</a>";
		$string .= "<a href=\"".URL."localcinema/cinema/".$venueData[$key]['venue_id']."/".$venueData[$key]['venue_id']."\">".$venueData[$key]['distance']."away<br/></a>";
		$string .= $cinemaPhone." || ";
		$string .= '<a href="'.urldecode($cinemaURL).'">Visit website</A>';

		} else {
			//$checkCount = $this->updateVenueCount($check['0']['cinemaid']);

/*
						//Updating the venue count if only one cinema selected...
						print $venueData['1']['venue_id'];			
						if(($key == 0) && !isset($venueData['1']['venue_id'])) {
						$this->updateVenueCount($check['0']['cinemaid']);
						print "Updating.";
						}			
*/
			
			$this->venues[$key] = $check;
			
			$string .= "<h4><a href=\"".URL."localcinema/cinema/".$check['0']['cinemaid']."\">".$check['0']['title']."</a></h4>";
			//$string .= "<a href=\"".URL."localcinema/cinema/".$check['0']['cinemaid']."\">What's on!</a>";
			$string .= "<a href=\"".URL."localcinema/cinema/".$venueData[$key]['venue_id']."/".$venueData[$key]['venue_id']."\">".$venueData[$key]['distance']." away<br/></a>";
			$string .= isset($check['0']['phone_number']) ? "".$check['0']['phone_number']." || " : "";
			$string .= "<a href=\"".$check['0']['link']."\">Visit website</a>";
			
		}

		
	}

	$this->areaCinemas = $venueData;
	
//	return $this->venueId;
	return $string;	
	}


	/*
		Only works after $this->venues is set by $this->getCinemas()
	*/
	public function getVenueTitle($id) {
		
		$venueInfo = $this->getVenue($id);
		if($venueInfo) {
			$ventitle = $venueInfo['0']['title'];	
			
		return $ventitle;
		} else {
			
		if($this->venues) {
			$result = array();
			foreach($this->venues as $key=>$value) {
				//print $this->venues[$key]['id'].'-';
				if(isset($this->venues[$key]['cinemaid']) && $this->venues[$key]['cinemaid'] == $id) {
					
					$result['title'] = isset($value['title']) ? $value['title'] : "";
					$result['distance'] = isset($value['distance']) ? $value['distance'] : "";
					$result['link'] = isset($value['link']) ? $value['link'] : "";
					$result['address'] = isset($value['address']) ? $value['address'] : "";
					$result['phone_number'] = isset($value['number']) ? $value['number'] : "";
					
				}
			}
	
				//$venkey = helper::recursive_array_search($id, $this->venues);
			//print '<h1>::'.$this->venues[$key]['title'].'::</h1><br/>';
		
				$ventitle = (isset($result['title'])) ? $result['title'] : $id;
			//$title = next($this->venues[$key]);
		//	print $venkey.'-'.$ventitle.'<br/>';

	} else {
		$ventitle = $id;
	}
	}
	return $ventitle;	
	}

	// private function tryDirect(){
	// $options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');
	
	// $cache = new cache($options);
	// $cache->setLifeTime((time() % 10));	//Seconds passed since current day	

	// 		//http://www.findanyfilm.com/find-cinema-tickets?#tabs-9355-2016-05-07
	// 		$url = 'http://www.findanyfilm.com/find-cinema-tickets?#tabs-9355';
	// 		$cinemaShowings = $cache->perform_cache($url,'tabs-9355'); 
			
	// 		var_dump($cinemaShowings);

	// 		exit;
	// }

	/*
		Take an array or an individual venueID, converts this to venues = array(id=>'', title=>'')return an array containing [key][title], [key][venue][venuekey][id]/[title],  [key][time][venuekey][timekey][time]...
		
	*/
	public function getShowings($venues,$var = null) {
	//$this->tryDirect();


	$options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');
	
	$cache = new cache($options);
	$cache->setLifeTime((time() % 86400));	//Seconds passed since current day	
	$films = array();

	//Looking at a specific cinema.
	if(is_string($venues)) {
		$venues = array(array(array('cinemaid' => $venues)));
	}

			foreach($venues as $vkey => $v) {	

				if(!isset($v['0']['cinemaid'])) {
					break;
				}
			
				$url = 'http://moviesapi.herokuapp.com/cinemas/'.$v['0']['cinemaid'].'/showings';
			#print $url.'<br/>';

			$cinemaShowings = $cache->perform_cache($url,'showing-'.$v['0']['cinemaid']); 
			
			if(@!$cinemaShowings['0']['title']) {
			
			#echo $url;
			print "<br/><br/>line 524 index_model - error - showings empty from <a href=$url>$url</a>";
			print_r($cinemaShowings);

			//$films = false;
			} else { //We're looking at all the films at venue $v;
				
					

			foreach($cinemaShowings as $key => $value) {
				//print_r($cinemaShowings);
				
				$title = preg_replace("/\([^)]+\)/","",$cinemaShowings[$key]['title']);	
				//print $title.'<br/>';
				
				
				$rtn = helper::in_array_r($title,$films);
					if($rtn == false) {  //Not found!
						
						$films[] = array('title' => $title, 'venue' => array(array('cinemaid' => $v['0']['cinemaid'])), 'time' => array($cinemaShowings[$key]['time']));		
					} else {
						$overRideKey = helper::recursive_array_search($title,$films);
						//It exists... so add the venue to the title.
						$films[$overRideKey]['venue'][] = array('cinemaid' => $v['0']['cinemaid']); //Add the array to the venue string

						$films[$overRideKey]['time'][] = $cinemaShowings[$key]['time']; //Add the times to the time string
					}

			}
			
			}

			//$this->scanIMDB($films['0']);
		//}
		}
		//print_r($films);
		//exit;
	return $films;


	}

	public function scanTrailers($idIMDB) {
		//print '<br/><br/>';
		if(!$idIMDB)
			break;
			
		if(substr($idIMDB,0,2) == 'tt')
			$idIMDB = substr($idIMDB, 2);
		

		$options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');		
		$cache = new cache($options);			
		$cache->setLifeTime(2592000);//30 days
	
//$url = "http://api.traileraddict.com/?imdb=".$idIMDB."&count=1&width=680";
//print $url;


		$url = 'http://api.traileraddict.com/?';
					$params = array( 
										'imdb' => $idIMDB,
										'count' => '1', 
										'width' => '680',
										);	
		
		// $url = 'http://trailersapi.com/trailers.json?';
		// 			$params = array( 
		// 								'movie' => $idIMDB,
		// 								'count' => '1', 
		// 								'width' => '680',
		// 								);	
		
		$url = $cache->set_url($url,$params);	
		//print urldecode($url);
//	print '<br/>'.$url.':<Br/><br/>';
	
		$trailerInfo = $cache->perform_cache($url,'trailer-'.$params['imdb'],true); 

		
		$trailer = array();
		$trailer['link'] = isset($trailerInfo['trailer']['link']) ? $trailerInfo['trailer']['link'] : "";
		$trailer['id'] = isset($trailerInfo['trailer']['trailer_id']) ? $trailerInfo['trailer']['trailer_id'] : "";
		
		return $trailer;


		
	//	print_r($trailerInfo['trailer']['link']['embed']);

/*
		$url = 'http://trailersapi.com/trailers.json?';
					$params = array( 
										'movie' => $title,
										'limit' => '3', 
										'width' => '640', 
										'format' => 'json'
										);	
		
		
		$url = $cache->set_url($url,$params);	
	//	print urldecode($url);
	print '<br/>'.$url.':<Br/><br/>';
	
		$trailerInfo = $cache->perform_cache($url,'TRAILER2-'.$params['movie']); 
 	
	if($trailerInfo['code']) {
	//print $trailerInfo['code'];
$doc = new DOMDocument();
@$doc->loadHTML($trailerInfo['code']);

$tags = $doc->getElementsByTagName('iframe');

foreach ($tags as $tag) {
       $trailerSrc = $tag->getAttribute('src');
}
*/


//	exit;
//print_r($trailerInfo);
 
	}

	public function scanIMDB($title) {

		$options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');
		$cache = new cache($options);			
		$cache->setLifeTime(2592000);//30 days

		//Need a token for payment at http://www.myapifilms.com/index.do - 5 euros.
		// $url = 'http://www.myapifilms.com/imdb?';
		// 			$params = array( 
		// 								'apikey' => 'b823080b';
		// 								'title' => $title,
		// 								'format' => 'JSON', 
		// 								'encoding' => 'json'
		// 								);	
		
		// $url = $cache->set_url($url,$params);
		

		$url = 'http://www.omdbapi.com/?i=&t='.$title;
		
//		print $url.'<Br/>';
		
		$filmInfo = $cache->perform_cache($url,'imdb-'.$title); 
		
		//$filmInfo = $filmInfo['content'];

			//print '<pre><br/>::::<br/><br/>'.print_r($filmInfo['0']).'<br/>::::<br/><br/>'.$filmInfo['title'].'</pre>';

		//OMDB api hasn't worked... 
		if( (isset($filmInfo['message']) && $filmInfo['message'] == 'Movie not found') || (!isset($filmInfo['title']) && !isset($filmInfo['Title']) ) || (isset($filmInfo['Error']) && $filmInfo['Error'] == 'No API key provided.') || $filmInfo['Response'] == 'False' ) {
			$filmInfo = "";
	
			//print $filmInfo['message'].'<br/>';
		
			//Scraping the IMDB website using the imdb.php library - and fudging the cache by saving over it...
			
			//print $cache->getFilename('imdb-'.$title);
		
			//Scraping IMDB...
			$movieArray = $this->imdbNo2->getMovieInfo($title);

// print helper::pr($movieArray);
// exit;

			//CAPITALISING FOR CONSISTENCY AND REMOVING DATA I DON'T WANT
			//
			if(isset($movieArray['title'])) {
				$movieArray['Title'] = $movieArray['title'];
			
				$movieArray['Response'] = true;
			}
			if(isset($movieArray['rating']))
				$movieArray['imdbRating'] = $movieArray['rating'];
			if(isset($movieArray['votes']))
				$movieArray['imdbVotes'] = $movieArray['votes'];
			if(isset($movieArray['title_id']))
				$movieArray['imdbID'] = $movieArray['title_id'];
			if(isset($movieArray['year']))
				$movieArray['Year'] = $movieArray['year'];
			if(isset($movieArray['poster']))
				$movieArray['Poster'] = $movieArray['poster'];
			if(isset($movieArray['plot']))
				$movieArray['Plot'] = $movieArray['plot'];
			if(isset($movieArray['runtime']))
				$movieArray['Runtime'] = $movieArray['runtime'];
			if(isset($movieArray['genres']))
				$movieArray['Genre'] = $movieArray['genres'];
			if(isset($movieArray['rated']))
				$movieArray['Rated'] = $movieArray['mpaa_rating'];
			if(isset($movieArray['release_date']))
				$movieArray['Released'] = $movieArray['release_date'];
			unset($movieArray['cast']);
			unset($movieArray['writers']);
			unset($movieArray['musicians']);
			unset($movieArray['also_known_as']);
			unset($movieArray['producers']);
// {"trailer":{"title":"Citizenfour: TV Spot - Academy","link":"http:\/\/www.traileraddict.com\/citizenfour\/tv-spot-academy","pubDate":"Mon, 16 Feb 2015 11:53:52 -0800","trailer_id":"99501","imdb":"4044364","embed":{}}}

		if(isset($movieArray['videos']['0'])) {
			$numID = (preg_replace('/[^0-9]/','',$movieArray['title_id']));
			$trailerfilename = 'trailer-'.$numID;	
			//print $trailerfilename;
					$trailer = array();	
					$trailer['link'] = $movieArray['videos']['0'];
					$trailer['id'] = $numID.' - unknown';	
					$movieArray['trailer'] = $trailer;
					$movieArray['Trailer'] = $trailer;	
					$trailer = json_encode($trailer);
					if($trailer !== false)
						$cache->save($trailer,$trailerfilename);
		}

			$movieJson = json_encode($movieArray);
			
			//Saving the data to a json cache
			$cache->save($movieJson,'imdb-'.$title);

//			print helper::pr($movieJson);
			
			//Overriting the filmInfo array to enable the next section..
			$filmInfo = $movieArray;
//			$filmInfo = json_decode($movieJson,true);
			
		 		//exit;

		// return false;
			//$filmInfo =  
		}


			if(isset($filmInfo['0']))
				$filmInfo = $filmInfo['0'];

		//ScanIMDB	//Insert API film data to DB.		
		$insertData = array();
		$insertData['title'] = isset($filmInfo['title']) ? $filmInfo['title'] : @isset($filmInfo['Title']) ? $filmInfo['Title'] : "";
		$insertData['rated'] = isset($filmInfo['rated']) ? $filmInfo['rated'] : @isset($filmInfo['Rated']) ? $filmInfo['Rated'] : "";
		$insertData['rating'] = isset($filmInfo['rating']) ? $filmInfo['rating'] : @isset($filmInfo['imdbRating']) ? $filmInfo['imdbRating'] : "";
		$insertData['runtime'] = isset($filmInfo['runtime']['0']) ? $filmInfo['runtime']['0'] : @isset($filmInfo['Runtime']) ? $filmInfo['Runtime'] : "";
		$insertData['votes'] = isset($filmInfo['votes']) ? $filmInfo['votes'] : isset($filmInfo['imdbVotes']) ? $filmInfo['imdbVotes'] : "";
		//$insertData['metascore'] = helper::checkIsset($filmInfo['metascore']);
		//$insertData['releaseDate'] = helper::checkIsset($filmInfo['releaseDate']);
		$insertData['year'] = isset($filmInfo['year']) ? $filmInfo['year'] : isset($filmInfo['Year']) ? $filmInfo['Year'] : "";
		//$insertData['image_filename'] = helper::checkIsset($filmInfo['image_filename']);
		$insertData['plot'] = isset($filmInfo['plot']) ? $filmInfo['plot'] : isset($filmInfo['Plot']) ? $filmInfo['Plot'] : "";
		$insertData['simple_plot'] = isset($filmInfo['simplePlot']) ? $filmInfo['simplePlot'] : $insertData['plot'];
		$insertData['id_imdb'] = isset($filmInfo['idIMDB']) ? $filmInfo['idIMDB'] : isset($filmInfo['imdbID']) ? $filmInfo['imdbID'] : "";
		$insertData['url_imdb'] = isset($filmInfo['urlIMDB']) ? $filmInfo['urlIMDB'] : "";

		
		//If the IMDB data isn't in the db...
	

		if($this->getFilm($insertData['id_imdb']) == false)
			$this->insertFilm($insertData);


		$insertData['image_filename'] = isset($filmInfo['Poster']) ? $filmInfo['Poster'] : "";
	
		// if($insertData['image_filename'] == "" && isset($filmInfo['idIMDB'])) {
		// 	$filmInfo['poster'] = $this->_checkPoster($filmInfo['idIMDB']);
		// 	$insertData['image_filename'] = 'http://image.tmdb.org/t/p/original'.$filmInfo['poster']['file'];
		//}
	
//print helper::pr($insertData);


		return $filmInfo;
		
		

	}

	// private function _checkPoster($idIMDB) {
	
	
	// if(isset($idIMDB)) {
		
	// $filmInDB = $this->getFilm($idIMDB);
	
	// $poster = array();
	// //Poster filename to append to this:
	// $poster['path'] = 'http://image.tmdb.org/t/p/original';
				
	
	// 	if($filmInDB !== false) {
		
		
	// 		if(isset($filmInDB['0']['image_filename']) && ($filmInDB['0']['image_filename'] !== "")) {
		
	// 			$poster['file'] = $filmInDB['0']['image_filename'];		
	// 			return $poster['path'].$poster['file'];
	// 			exit;
	// 		} else {
	// 			//Files in the db, but no postername...
	// 			//print "HRrr";

	// 			$options = array('cacheDir' => ROOT.APPS.APP_NAME.'tmp/');
	// 			$cache = new cache($options);			
	// 			$cache->setLifeTime(2592000);//30 days2592000
	// 			$url = 'https://api.themoviedb.org/3/find/'.$idIMDB.'?';
	// 						$params = array( 
	// 											'api_key' => '1dea1a70ffaface0c665f4abe0460d66', 
	// 											'external_source' => 'imdb_id'
	// 											);	
	// 			$url = $cache->set_url($url,$params);
	// 			$posterInfo = $cache->perform_cache($url,'poster-'.$idIMDB); 
				

	// 			print $url.'<Br/>';
	// 			print_r($posterInfo);

				
		
	// 			$poster['file'] = isset($posterInfo['movie_results']['0']['poster_path']) ? $posterInfo['movie_results']['0']['poster_path'] : "";
		
	// 		//	print $poster['file'];
		
	// 				if(isset($poster['file'])) {
	// 					$this->updateFilmPoster($idIMDB,$poster['file']);
				
			
	// 				return $poster['path'].$poster['file'];
	// 				exit;
	// 				}
				
	// 		}
		
		
	// 	//return $poster;
	// 	} //Otherwise no film in the db... something has gone wrong.
	
	// }
	// return false;
	// }

	/**
	 * Take an array of film names... get details by scanIMDB...
	 */
	public function renderRating($allFilms,$details = null) {	
		
		$output = array();
		//foreach($allFilms as $key=>$value)
		//	$this->filmTitles[] = $this->showings[$key]['title'];
/*
			
		print_r($allFilms);
		exit;
			
*/
		$ratings = array();
		
		if(isset($details))  {
		print "+";
			$ratings[] = $this->scanIMDB($allFilms);


			$this->output['title'] = $ratings['0']['title'];
			$this->output['rating'] = $ratings['0']['rating'];
			$this->output['votes'] = $ratings['0']['votes'];
			
//			copy(''.$ratings['0']['urlPoster'].'', 'tmp/'..'');

			//$this->output[$filmkey]['poster'] = $this->_checkPoster($ratings['0']['idIMDB']); 
		
		
		//	$poster = $this->_checkPoster($ratings['0']['idIMDB']);
			$this->output['poster'] = isset($ratings['0']['poster']) ? $ratings['0']['poster'] : "";
			
			//$this->output['poster'] = '<img src=\''.$imagedir2.$imagefilename.'\' alt=\''.$ratings['0']['title'].'\' title=\''.$ratings['0']['title'].'\'/><br/>';
			
			$this->output['simplePlot'] = $ratings['0']['simplePlot'];
			$this->output['year'] = $ratings['0']['year'];
			$this->output['IMDB'] = $ratings['0']['idIMDB'];
	
			$this->updateFilmDate($this->output['IMDB']);
	
	http://image.tmdb.org/t/p/original
			
			$this->output['urlIMDB'] = $ratings['0']['urlIMDB'];
			
			
			exit;				
		}
		
		
		/*allFilms is an array comprising: e.g.
(
    [0] => Array
        (    [title] => Blade Runner: The Final Cut  
            [venue] => Array
                ([0] => Array
                        ([id] => 2432, [title] => 'vue islington' )

                    [1] => Array
                        ([id] => 6902, [title] => 'curzon bloomsbury' )
                 )
            [time] => Array
                (  [0] => Array
                        (   [0] => 15:10   )

                    [1] => Array
                        (    [0] => 22:15)
                ) )
		*/
		
//			print helper::pr($allFilms);
		foreach($allFilms as $filmkey => $film) {


			$this->output[$filmkey]['title'] = $allFilms[$filmkey]['title'];
				/*
					Venue/Timing array for the particular film: $film['venue']
				*/
				$filmVenues = array(); 
				

				// print helper::pr($film);
				// exit;

				foreach($film['venue'] as $key => $cinemaid)
					foreach($film['time'][$key] as $tkey => $time)
						$filmVenues[$cinemaid['cinemaid']][] =$time;

//print $allFilms[$filmkey]['title'].print_r($filmVenues);
//print_r($filmVenues);

/*
				$filmTimes = array(); 
				foreach($film['time'] as $key => $time)
					foreach($time as $t)
						$filmTimes[] .= $t;
*/

					$i = "";	
					$venueshowings = array();
					foreach ($filmVenues as $venue => $timeArray) {

						//if(isset($f) && $f == $allFilms[$filmkey]['title'])
						//	break;

//Filmkey
	//Venuekey
		//Showingkey
										
						foreach($timeArray as $time) {
							if($time > $this->timeFilter) {
								
								if($i == "") {

/*								print '<br/>new film';
								print '</br>'.$allFilms[$filmkey]['title'].'</br>';
								print $this->getVenueTitle($venue);
								print $time;
								
*/
								$venuecount = 0;
								$venueshowings[] = array('id' => $venue, 'title' => $this->getVenueTitle($venue));
								$venueshowings[$venuecount]['time'] = array($time);
								} elseif($i !== $venue) {
								
								$venuecount = $venuecount+1; 
/*
								print "<br/>new venue";
								print '</br>'.$this->getVenueTitle($venue);
								print $time;
								
*/
								$venueshowings[] = array('id' => $venue, 'title' => $this->getVenueTitle($venue));
								$venueshowings[$venuecount]['time'][] = $time;
								
								
								} else {
/*
									print "new time";
									print ','.$time;
*/
									
								@$venueshowings[$venuecount]['time'][] = $time;
								} 

 

// 							}			

						$this->output[$filmkey]['showings'] = $venueshowings;						
								
					
	//print_r($venueshowings);
	//print '<Br/><br/>';
								$i = $venue;
								}//End time filter
						}

					$f = $allFilms[$filmkey]['title'];	
					}
					
//	print_r($this->output);				

					//print $venueString;
				//	$this->output[$filmkey]['venues'] = $venueString;

//print $film['title'];
			$data = $this->scanIMDB($film['title']);
				
				//print helper::pr($data);	

				
			$this->output[$filmkey]['idIMDB'] = isset($data['idIMDB']) ? $data['idIMDB'] : "";
			if(($this->output[$filmkey]['idIMDB'] == "") && isset($data['imdbID'])) {
				$this->output[$filmkey]['idIMDB'] = $data['imdbID'];
			}

			$this->output[$filmkey]['rating'] = isset($data['rating']) ? $data['rating'] : "0";
			
			if($this->output[$filmkey]['rating'] == "0") {
				$this->output[$filmkey]['rating'] = (isset($data['imdbRating'])) ? $data['imdbRating'] : "0";
			}
			
			//print  $film['title'].':'.$this->output[$filmkey]['rating'].':<br/>';
			
			$this->output[$filmkey]['simplePlot'] = isset($data['plot']) ? $data['plot'] : "";

			if(isset($data['Plot']) && $this->output[$filmkey]['simplePlot'] == "")
				$this->output[$filmkey]['simplePlot'] = $data['Plot'];
			

			$this->output[$filmkey]['year'] = isset($data['year']) ? $data['year'] : "";
			if($this->output[$filmkey]['year'] == "") {
				$this->output[$filmkey]['year'] = isset($data['Year']) ? $data['Year'] : "";
			}

			if(isset($data['votes']))
				$this->output[$filmkey]['votes'] = $data['votes'];
			if(isset($data['Votes']))
				$this->output[$filmkey]['votes'] = $data['Votes'];


//var_dump($this->output[$filmKey]);


			if(isset($data['idIMDB'])) {
		
				$this->updateFilmDate($data['idIMDB']);
			}
				
				//$poster = $this->_checkPoster($data['idIMDB']);
				if(isset($data['poster'])) {
					$this->output[$filmkey]['poster'] = $data['poster'];
				} elseif (isset($data['Poster'])) {
					$this->output[$filmkey]['poster'] = $data['Poster'];
				} else {
					$this->output[$filmkey]['poster'] = "";
				}

				if(isset($data['Trailer']['link']))
					$this->output[$filmkey]['trailer'] = $data['Trailer']['link'];

			if(isset($data['urlIMDB']))
				$this->output[$filmkey]['urlIMDB'] = $data['urlIMDB'];
						
			//IMDB search worked
			if(($data !== false) && ($this->output[$filmkey]['rating'] !== "")) {
								
				$ratings[] = array('rating' => $this->output[$filmkey]['rating'], 'title' => '<a href="'.URL.'localcinema/film/'. urlencode($film['title']).'">'.$film['title'].'<a/>', 'plot' => $this->output[$filmkey]['simplePlot'],'year' => $this->output[$filmkey]['year'], 'venues' => $venueshowings);
			} else { //IMDB search failed.
		
				$ratings[] = array('rating' => '0', 'title' => $film['title'], 'time' => array($film['time']), 'venues' => $venueshowings);
			}
			

		}


//var_dump($ratings);

//print_r($this->output);	
		if(isset($this->output['distance'])) {
			$sortcolumn = 'distance';
			helper::array_sort_by_column($this->output, $sortcolumn, SORT_NUMERIC);
		} else {
			$sortcolumn = 'rating';
			helper::array_sort_by_column($this->output, $sortcolumn, SORT_DESC);
		}
		
		//print "Uncomment";
//		if(is_array$this->output !== null) //If no films at a venue, for eg.
//if(isset($sortcolumn))
		
		//helper::array_sort_by_column($ratings, 'rating', SORT_DESC);


		//var_dump($this->output);
		//exit;
		
		$string = "";

		foreach ($ratings as $key => $value) {
// 			$string .= '<h3>'.$ratings[$key]['rating'] .':'. $ratings[$key]['title'].'</h3> ('.$ratings[$key]['year'].')<br/>';

			$string .= (isset($ratings[$key]['rating'])) ? '<h3>'.$ratings[$key]['rating'] : '<h3>';
					
			$string .= (isset($ratings[$key]['title'])) ? ':'. $ratings[$key]['title'].'</h3>' : '</h3>';
			
			$string .= (isset($ratings[$key]['year'])) ? '('.$ratings[$key]['year'].')<br/>' : '<br/>';



			//$string .= "<span><h6>".$ratings[$key]['venues'].'</h6></span>';
		}
	
			return $string;	
		
	}
}

	

?>