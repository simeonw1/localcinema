<?php 
class index extends controller {

private $message;

	function __construct() {
		parent::__construct();
		
		//If secure
	//	auth::handleLogin(); 		
		$this->view->js = array("foundation/js/app.js","js/vendor/modernizr.js","js/vendor/jquery.js"); 
//		$this->view->js = array('js/foundation/foundation.accordion.js');

	#define('APP_NAME', 'localcinema/');
	
	$this->view->message = "";
	$this->view->siteTitle = "Movie Monitor";
	$this->view->filmTemplate = "";
	//define('APPINSTALLED', '');
	//Uncomment when uploading to the server.
	@define('APPINSTALLED', '../'.APPS.APP_NAME.'views/');
	}
 
/*
print '<br/><br/>';
print 'http://www.google.com/movies?hl=en&near=arlington+heights&ei=Ws4-T_r2AsPO2wWflp23CA&view=list';
print 'https://code.google.com/p/aenext-tickit-cinema-api/';
print 'https://code.google.com/p/aenext-melon-tickit/';
*/

	
	public function search($location) {
		
		$postcode = $this->model->search($location);
		
		if($postcode == false) {
			$this->setMessage("Couldn't locate a postcode");
			return false; 
		} else { 
			return $postcode; 
		}
	}
	
	public function setMessage($string) {
		
		if(!isset($this->message)) {
			$this->message = $string;
			} else {
			$this->message = $this->view->message.'<br/>'.$string;
			}
			$this->view->message = "<div data-alert class=\"alert-box info radius\">".$this->message."</div>";
			
		return $this->view->message;
	}

	
	private function _processCinemaInfoTemplate($output,$key) {

			$this->view->id = $output[$key]['venue_id'];					
			$this->view->distance = (isset($output[$key]['distance'])) ? $output[$key]['distance'].' away' : "";
			$this->view->address = $output[$key]['address'];
			$this->view->phone_number = (isset($output[$key]['phone_number'])) ? $output[$key]['phone_number'] : "";
			$this->view->link = (isset($output[$key]['link'])) ? urldecode($output[$key]['link']) : "";
		$this->view->title = (isset($output[$key]['name'])) ? $output[$key]['name'] : 'Title';


	$this->view->accordionName =  isset($this->view->title) ? '<h3 class="subheader">  '.$this->view->title .'</h3>' : "";
	

	if($key == 0 && !isset($output['1']['venue_id'])) {
		$this->view->accordionClass = "active";
		$this->model->updateVenueCount($this->view->id);
		//print "Updating";
	} else {
		$this->view->accordionClass = "";
	}

	$address = $output[$key]['address'];
	$popVenuePostCodeArr = explode(',', $address);
	$popVenuePostCode = end($popVenuePostCodeArr);
	if(strlen($popVenuePostCode) > 3)
	$check = postcode::inspect($popVenuePostCode);
	if(isset($check['validate']) && $check['validate'] == true) {
		$venuePostcode = $check['prefix'].' '.$check['suffix'];

		$latlng = postcode::getLatLng($venuePostcode);			

		if(!$latlng == false) {
				
			$this->view->latitude = $latlng['latitude'];
			$this->view->longitude = $latlng['longitude'];
			$this->view->latlng = '['.$this->view->latitude.','.$this->view->longitude.']';

		$this->addCss($this->view->panelName);

		$this->view->mapTemplate = $this->view->parse("".ROOT.APPS.APP_NAME."views/map.php");	

		}

	}
		
		$this->view->filmTemplate .= $this->view->parse("".ROOT.APPS.APP_NAME."views/venuetemplate.php");	

	}
	
	private function addCss($cssName) {
	if(isset($cssName))
		$string = <<<HEREDOC
		#mappage$cssName {	
			height:40px;top:0px;background-position:center bottom;text-align:center;vertical-align:middle;height:200px;width:250px}
HEREDOC;
		
			if(!isset($this->view->extraCss)) {
			$this->extraCss	= $string;	
			} else {
			$this->extraCss .= $string;
			}
			$this->view->extraCss = "<style>".$this->extraCss.'</style>';
			
		return $this->view->extraCss;
	}
	
	private function _getRGB($id) {
		 $i = $id;
	    switch(true)
	    {
	        case $i > 8.0:
	            
	            $rgb = helper::GreenYellowRed($i*10);
	            break;
	               
	        case $i > 7.0:
	            
	            $rgb = helper::GreenYellowRed($i*7.5);
	          break; 
	        case $i > 6.0:
	            
	            $rgb = helper::GreenYellowRed($i*5.5);
	            break;

	        case $i > 5.0:
	            
	            $rgb = helper::GreenYellowRed(30);
	            break;
	            
	        // 2nd column
	        case $i > 5:
	            
	            $rgb = helper::GreenYellowRed(20);
	            break;
	  
	                
	        // this should never happen
	        default:
	            $rgb = helper::GreenYellowRed(10);
	            break;
	    }
    //(string) helper::rgb2hex($rgb)
    return $rgb;
		
	}
	
	private function _processFilmRatingTemplate($output,$key) {
	 //Rating page
				
				//print_r($output['1']['rating']);
				//exit;
	if($key == 0 && !isset($output['1']['rating'])) {
		$this->view->accordionClass = "active";
		
	} else {
		$this->view->accordionClass = "";
	}			
				
//				var_dump($output);
				$this->view->rating = $output[$key]['rating'];


				if(helper::numeric($this->view->rating) !== false)
					$this->view->rgb = (string) $this->_getRGB($this->view->rating);

				if($this->view->rating == '0')
					$this->view->rating = "TBC";
						
			$this->view->votes = isset($output[$key]['votes']) ? '('.$output[$key]['votes'].' votes)' : "";
			$this->view->urlIMDB = isset($output[$key]['urlIMDB']) ?
			'<a href="'.$output[$key]['urlIMDB'].'"><strong>See more from IMDB</strong></a>' : "";
			
			$this->view->poster = (isset($output[$key]['poster'])) ? ''.$output[$key]['poster'] : URL.APPS.APP_NAME.'public/roll.jpg';
		
			$this->view->filmTitle = (isset($output[$key]['title'])) ? $output[$key]['title'] : '';
		
			$this->view->idIMDB = (isset($output[$key]['idIMDB'])) ? $output[$key]['idIMDB'] : '';


			if(isset($output[$key]['trailer'])) {
				$this->view->trailer['link'] = $output[$key]['trailer'];
			} else {
	
				if($this->view->idIMDB) {
				$trailers = $this->model->scanTrailers($this->view->idIMDB);			
				$this->view->trailer = $trailers;
				
				}

			}
			
			$this->view->simplePlot = (isset($output[$key]['simplePlot'])) ? $output[$key]['simplePlot'] : "";

			
			$this->view->year = (isset($output[$key]['year'])) ? '('.$output[$key]['year'].')<br/>' : '<br/>';
	
   	///$this->view->rgb

			if($key == 0) {
				$this->view->accordionName = '<h3 class="subheader">'.$this->view->rating.'<span data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Click on me to find out more.">&nbsp;&nbsp;:&nbsp;&nbsp;</span>'.$this->view->filmTitle . ' '.$this->view->year.'</h3>';
			} else {
			$this->view->accordionName =  '<h3 class="subheader">  '.$this->view->rating.' : '.$this->view->filmTitle .' '. $this->view->year.'</h3>';

			}
		
		
	}
	
	private function _processFilmShowings($output,$key) {
		
			$this->view->showings = "";
			$this->view->showings .= isset($output[$key]['showings']) ? "<h3>Showing today:</h3>" : ""; 
			
			
			if(!@$output[$key]['showings']) {
					return false;
					exit;
				} else {
					
/*
					print_r($output[$key]['showings']);
					exit;
*/

					if (is_array($output[$key]['showings'])) {
					foreach($output[$key]['showings'] as $key => $value) {
						
						
						//$this->view->showings .= '<a href="'.URL.APP_NAME.'cinema/'. @$value['id'].'">'.@$value['title'].'</a></br>';
						$this->view->showings .= '<a href="'.URL.APP_NAME.'cinema/'. @$value['id'].'/'.@$value['id'].'/'.@$this->var.'">'.@$value['title'].'</a></br>';
						
						
						foreach($value['time'] as $key => $showing)
							$this->view->showings .= '<a href="#" class="tiny button">'.$showing.'</a>';
					
					//New venue
					$this->view->showings .= "<br/>";
					}
					}
				}
	return $this->view->showings;	
	}
	
	private function _processResultTemplate() {
		$output = $this->model->output;		

		$this->view->filmTemplate = "";
		
/*
		print_r($output);
		exit;
*/
		
		//if(is_array($output)) {
		foreach ($output as $key => $value) {
			
			if(isset($output[$key]['rating'])) { //Showing the films...
			$this->view->panelName = 'fPanel'.$key;
			
				
			
				$this->_processFilmRatingTemplate($output,$key);
				$showingToday = $this->_processFilmShowings($output,$key);
				
				if($showingToday !== false)
					$this->view->filmTemplate .= $this->view->parse("".ROOT.APPS.APP_NAME."views/filmtemplate.php");	
				
			} else { //Listing venues (either single if /postcode/vID or all if not...)
			$this->view->panelName = 'vPanel'.$key;

				$this->_processCinemaInfoTemplate($output,$key);
			//exit;
			}

		}
		//}
	}




	public function about() {

		$this->view->title = "Movie Monitor";

		
		$this->view->load(APPINSTALLED."header");	

		$this->view->render('../'.APPS.APP_NAME.'views/about');
		$this->view->load(APPINSTALLED."footer");	

	} 	

	public function index($var = null) {

		$this->view->title = "Movie Monitor";

	////TOP VENUES
		$topVenues = $this->model->listTopVenues(7);
		
		if($topVenues !== false) {
			$this->view->topVenues ='<a href="'.URL.'localcinema/cinema"><h3 class="subheader">Popular Cinemas</h3></a>
			<ul>';

		foreach($topVenues as $key => $value)
			$this->view->topVenues .= '<li><a href="'.URL.'localcinema/cinema/'.$topVenues[$key]['cinemaid'].'">'.$topVenues[$key]['title'].'</a></li>';

			$this->view->topVenues .= '</ul>';
		}		
	////TOP VENUES				
	/////TOP GROUP
			$topFilms = $this->model->listTopFilms(7,date("Y-m-d",strtotime("-1 month")));
		
		if($topFilms !== false) {
			$this->view->topFilms ='<a href="'.URL.'localcinema/film"><h3 class="subheader">Top Films</h3></a>
			<ul>';

		foreach($topFilms as $key => $value)
			$this->view->topFilms .= '<li><a href="'.URL.'localcinema/film/'.$topFilms[$key]['id_imdb'].'">'.$topFilms[$key]['title'].' ('.$topFilms[$key]['rating'].')</a></li>';

			$this->view->topFilms .= '</ul>';
		}
	/////TOP GROUP
	
		
		$this->view->load(APPINSTALLED."header");	
		$form = new view();
	$this->view->set("form", $this->view->parse("".ROOT.APPS.APP_NAME."views/form.php"));	
	
		$this->view->render('../'.APPS.APP_NAME.'views/index');


		//$this->view->render('../apps/testApp/views/index');
		$this->view->load(APPINSTALLED."footer");	

	} 					

	public function results() {
		

		$this->_processResultTemplate();
		
		$this->view->load(APPINSTALLED."header");
		$this->view->render('../'.APPS.APP_NAME.'views/results');
		$this->view->load(APPINSTALLED."footer");	

	} 	
			

	// //Google showtimes discontinued
	// public function google($location) {
		

	// 	$this->g = new showtimes();
	// 	$showtimes = $this->g->get($location);

	// 	var_dump($showtimes);

	// } 	



	public function cinema($var = null,$var2 = null) {
	
	$this->var =  urldecode($var);
	
		//If the second var is set, display a single cinema based on the ID specified in var2
		if(isset($var2)) {			
			$this->var2 = $var2;
			$singleVenue = $this->model->getVenue($this->var2);
			
			if($singleVenue !== false)

				$this->view->pagetitle = "Single Venue Information ";//. //$singleVenue['0']['title'];

				$this->model->output = $singleVenue;
				$this->model->output['0']['venue_id'] = $this->var2;

		//$this->view->results = $this->cinemas;
		
		$this->results();
		exit;		
		}
	
		
	if($this->var) {

		$this->showings = $this->model->getShowings($this->var);

			$this->view->results = $this->model->renderRating($this->showings);

		$this->view->pagetitle = "Films currently showing at ". $this->model->getVenueTitle($this->var);

	$this->results();
	exit;		
	}
			
	$this->index();
	}

	//Var = postcode //var2 = cinemaID
	public function area($var = null,$var2 = null) {

	$this->var = urldecode($var);
	
	//override if post submitted...
	if(isset($_POST['location']))
		$this->var =  $_POST['location'];
		
/*
	$this->var = $this->search($this->var);
		if($this->var == false) {
		$this->index();
		exit;
		}
*/

		$this->view->pagetitle = "Showing cinemas near your area: ". ucfirst($this->var);
	
		$this->cinemas = $this->model->getCinemas($this->var);
		$this->model->output = $this->model->areaCinemas;
/*
	
		print_r($this->cinemas);
		exit;
*/
		if(!isset($this->cinemas) || $this->cinemas == false) {
			$this->setMessage("Failed to locate cinemas. Try a postcode.");
			$this->index();
			exit;
			}


			
		if(isset($var2)) {
			$this->var2 = $var2;
			$singleVenue = $this->model->getVenue($this->var2);
			
			if($singleVenue !== false)
				$this->view->pagetitle = "Single Venue Information ";//. //$singleVenue['0']['title'];

				$this->model->output = $singleVenue;
				$this->model->output['0']['venue_id'] = $this->var2;

				//print_r($this->model->output);
		}

		$this->view->results = $this->cinemas;

	$this->results();

	}
	
	public function all($var = null, $filter = null) {
	if(isset($filter) && (helper::numeric($filter) !== false))
		 $this->model->setTimeFilter($filter);

	$this->var = urldecode($var);
	
	//override if post submitted...
	if(isset($_POST['location']))
		$this->var =  $_POST['location'];
		
/*
	$this->var = $this->search($this->var);
		if($this->var == false) {
		$this->index();
		exit;
		}
*/


		$this->view->pagetitle = "Showing all screenings left today near: ". ucfirst($this->var);
	
		/*
			$this->model->venues provides a list of all cinemas - array[key][id] / array[key][title]
			getCinemas returns a html output.
		*/
		
		$this->cinemas = $this->model->getCinemas($this->var);
	
/*
	print_r($this->var);
	print_r($this->cinemas);
	exit;
*/
		
		if(!isset($this->cinemas) || $this->cinemas == false) {
			$this->setMessage("Failed to locate cinemas. Try a postcode.");
			
			$this->index();
			exit;
			}
	
			$this->showings = $this->model->getShowings($this->model->venues,'all');
	

//		$this->allFilms = array_unique(array_merge($this->allFilms,$this->filmTitles), SORT_REGULAR);
		
			$this->view->results = $this->model->renderRating($this->showings);
			//print_r($this->view->results);
			
			$this->view->leftbar = $this->cinemas;

	$this->results();

	}

	public function film($id_imdb = null) {
	
	$this->id_imdb = isset($id_imdb) ? urldecode($id_imdb) : "";
	
	
		//If the second var is set, display a single cinema based on the ID specified in var2
		if(isset($id_imdb)) {			
			$filmInfo = $this->model->getFilm($this->id_imdb);
			$this->view->pagetitle = "Film Information ";//. //$singleVenue['0']['title'];

		} else {
		
			$filmInfo = $this->model->listTopFilms(15,date("Y-m-d",strtotime("-7 days")));
			$this->view->pagetitle = "All the top rated films showing in the UK this week";

		}	
			if($filmInfo !== false) {

/*
				print_r($singleFilm);
				exit;
*/

			foreach($filmInfo as $key => $value) {

/*******REFACTOR****/
				$filmInfo[$key]['idIMDB'] = $filmInfo[$key]['id_imdb'];
				$filmInfo[$key]['urlIMDB'] = $filmInfo[$key]['url_imdb'];
				$filmInfo[$key]['simplePlot'] = $filmInfo[$key]['plot'];
				$filmInfo[$key]['poster'] = isset($filmInfo[$key]['image_filename']) ? ''.$filmInfo[$key]['image_filename'] : URL.APPS.APP_NAME."public/roll.jpg";
				/*******REFACTOR****/				
				
					$this->view->panelName = $filmInfo[$key]['idIMDB'];
	
				$this->_processFilmRatingTemplate($filmInfo,$key);
				
/*
				print_r($filmInfo);
				exit;
*/
				
				$this->view->filmTemplate .= $this->view->parse("".ROOT.APPS.APP_NAME."views/filmtemplate.php");	
						
					}
				
				
				
				}
				//$this->model->output['0']['venue_id'] = $this->var2;

		//$this->view->results = $this->cinemas;
		
				$this->view->message .= @$this->cache->message;

		$this->view->load(APPINSTALLED."header");
		$this->view->render('../'.APPS.APP_NAME.'views/results');
		$this->view->load(APPINSTALLED."footer");
		exit;		
		}
	




}

?>